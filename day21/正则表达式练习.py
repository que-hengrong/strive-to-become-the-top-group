import re

# 匹配邮箱
ret = re.match("[A-Za-z_]{4,20}@163\.com$", "hello@163.com")
if ret:
    print(ret.group())
else:
    print('no match substr')

# 匹配区号
ret1 = re.match("([^-]+)-(\d+)", "010-1234567")
print(ret1.group())
print(ret1.group(0))
print(ret1.group(1))

# 匹配不是以4，7结尾的手机号
ret2 = re.match("\d{10}[0-35-68-9]", "12345678901")
print(ret2.group())

# # 匹配0-100的数
# ret3 = re.match("[1-9]?\d$|100", "8")
# print(ret3.group())

# 匹配出<html><h1>hh</h1></html>
ret4 = re.match(r"<(\w+)><(\w+)>\w+</\2></\1>", "<html><h1>hh</h1></html>")
print(ret4.group())

ret5 = re.match(r"<(?P<name1>\w*)><(?P<name2>\w*)>.*</(?P=name2)></(?P=name1)>",
                "<html><h1>www.cskaoyan.com</h1></html>")
print(ret5.group())
