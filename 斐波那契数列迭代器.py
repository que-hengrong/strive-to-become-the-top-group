import time


class fibonacci:
    def __init__(self, n):
        self.n = n
        self.cur = 0
        self.num1 = 0
        self.num2 = 1

    def __next__(self):
        if self.cur < self.n:
            self.cur += 1
            self.num1, self.num2 = self.num2, self.num1 + self.num2
        else:
            raise StopIteration

    def __iter__(self):
        return self


if __name__ == '__main__':
    f = fibonacci(10)
    for i in f:
        time.sleep(1)
        print(f.num1)
