# 		git上传与下载

## 下载：

​	1.git clone ssh地址

## 上传三部曲：

​	1.git init #创建本地仓库

​	2.git remote add 仓库名称 仓库ssh地址 #关联本地仓库

​	3.git add 文件名

​	4.git commit -m "注释信息"

​	5.git pull origin master --rebase #同步远程仓库内容

​	4.git push origin master

## 遇到可能报错：

![image-20240428112055105](C:\Users\12280\AppData\Roaming\Typora\typora-user-images\image-20240428112055105.png)

### 1.需要添加公钥到码云ssh设置中

​	密钥生成方法：

​		ssh-keygen.exe一直回车，生成公钥，和私钥

2.

![image-20240428112744710](C:\Users\12280\AppData\Roaming\Typora\typora-user-images\image-20240428112744710.png)

本地与远程仓库版本不一致需要更新



3.

![image-20240428113510632](C:\Users\12280\AppData\Roaming\Typora\typora-user-images\image-20240428113510632.png)

暂存区没内容或者已经被git监测

需要修改add的任意文件内容即可

4.

![image-20240428114313482](C:\Users\12280\AppData\Roaming\Typora\typora-user-images\image-20240428114313482.png)

没有将本地仓库和远程仓库进行关联