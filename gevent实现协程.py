import gevent
from gevent import monkey

# 非常重要，一定要在程序之前打补丁
#111111test
monkey.patch_all()


# 相比greenlet优势：
# 完全实现自动切换协程
def f(n):
    for i in range(n):
        print(gevent.getcurrent(), i)
        gevent.sleep(1)


g1 = gevent.spawn(f, 5)
g2 = gevent.spawn(f, 5)
g3 = gevent.spawn(f, 5)

g1.join()
g2.join()
g3.join()
