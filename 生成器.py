# 生成器借助yield实现

def fib(n):
    cur = 0
    num1, num2 = 0, 1
    while cur < n:
        cur += 1
        num = num1
        num1, num2 = num2, num1 + num2
        yield num
        # 每次在此处中断，需要next唤醒继续
    return 'done'


def gen():
    i = 0
    while i < 5:
        temp = yield i
        print(temp)
        i += 1


if __name__ == '__main__':
    # F = fib(5)
    # print(next(F))
    # print(next(F))
    # print(next(F))
    # print(next(F))
    g = gen()
    print(next(g))
    print(next(g))
    print(g.send('haha'))
    print(next(g))
