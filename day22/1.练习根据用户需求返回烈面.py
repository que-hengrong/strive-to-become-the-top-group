import socket
import re


def client_service(new_client: socket):
    request = new_client.recv(1024).decode('utf8')
    print(request)
    request_lines = request.splitlines()
    print(request_lines)
    if request_lines:
        file_name = ""
        # 取url
        ret = re.match(r"[^/]+(/[^ ]*)", request_lines[1])
        if ret:
            file_name = ret.group(1)

    # 返回http格式的数据给浏览器
    try:
        file = open('./html' + file_name, 'rb')
    except:
        responce = "HTTP 1.1 404 NOT FOUND\r\n"
        responce += "\r\n"
        responce += "-------file not found-------"
        new_client.send(responce.encode('utf8'))
    else:
        html_content = file.read()
        responce = "HTTP 1.1 200 OK\r\n"
        responce += "\r\n"
        new_client.send(responce.encode('utf8'))
        new_client.send(html_content)

def main():
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    addr = ("192.168.231.1", 7890)
    tcp_server_socket.bind(addr)
    tcp_server_socket.listen(128)
    new_client, client_addr = tcp_server_socket.accept()
    client_service(new_client)


if __name__ == '__main__':
    main()
