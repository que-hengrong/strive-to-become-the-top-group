from collections.abc import Iterable


# 定义一个可迭代对象
class Mylist:

    def __init__(self):
        self.container = []

    def add(self, item):
        self.container.append(item)

    # iter函数用于获得可迭代对象的迭代器
    def __iter__(self):
        my_iterator = iterator(self)
        return my_iterator


# 定义一个迭代器
class iterator:
    def __init__(self, mylist):
        self.mylist = mylist
        # current用来记录遍历位置
        self.current = 0

    def __iter__(self, val):
        # 迭代器的迭代器就是它自己
        return self

    def __next__(self):
        # 实现了遍历可迭代对象元素的过程
        if self.current < len(self.mylist.container):
            temp = self.mylist.container[self.current]
            self.current += 1
            return temp
        else:
            raise StopIteration


if (__name__ == '__main__'):
    mylist = Mylist()
    mylist.add(1)
    mylist.add(2)
    mylist.add(3)
    print(isinstance(mylist, Iterable))
    for i in mylist:
        print(i)
