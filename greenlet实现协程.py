from greenlet import greenlet
import time

# 相比yield好处：
# 不用一直next，只需要switch
# 即可跳转到另一个函数
def work1():
    while True:
        print('----work1----')
        w2.switch()
        time.sleep(0.5)


def work2():
    while True:
        print('----work2----')
        w1.switch()
        time.sleep(0.5)

w1 = greenlet(work1)
w2 = greenlet(work2)

w1.switch()
