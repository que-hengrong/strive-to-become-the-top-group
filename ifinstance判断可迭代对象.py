from collections.abc import Iterable
# python 3.3后需要从collections.abc导入！


if __name__ == '__main__':
    list1 = [1, 2, 3, 4]
    tuple1 = (1, 2, 3, 4)
    print(isinstance(list1, Iterable))
    print(isinstance(tuple1, Iterable))
